﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.4024.11">
  <POU Name="PILSv2" Id="{90d527b3-98ed-47d0-a704-81d6444f3844}" SpecialFunc="None">
    <Declaration><![CDATA[PROGRAM PILSv2
VAR
    // Indexer variables
    nDevices: WORD;
    nDevNum: WORD;
    nInfoType: WORD;
    bPILSV2isInitialized: BOOL := FALSE;
    nITemp: BYTE;
    nTempofs: WORD;
    fbTonStartDelay: TON;
    nPLCopenStateMachine1: WORD := 0;
// Persistent stuff up to %MB62
    fMagic AT %MB0: REAL := 2015.02; //Version of PILS, must be 2014.0 - 2046.0
    nOffset AT %MB4: WORD := 64; //Pointer to which byte address of Indexer
    stIndexer AT %MB64: ST_Indexer;
    nCycle: WORD := 0;
END_VAR
]]></Declaration>
    <Implementation>
      <ST><![CDATA[fbTonStartDelay(IN:=TRUE,PT:=T#4S);
IF fbTonStartDelay.Q THEN
    fbMotorM1(stAxisStruct:= astAxes[1], stDeviceStructPILS:= stMotorM1, stAxisPar:= stMotorM1Param);

    GVL_PILS.stUTCEL1252P1.nValue := GVL_APP.nUTCEL1252P1;
    GVL_PILS.stUTCEL1252P1.nStatus := GVL_APP.nPTPErrorStatusBits;
    GVL_PILS.stUTCEL1252P1.nErrorID := 0;
    GVL_PILS.stUTCEL1252P1.nReserved := 0;
    GVL_PILS.stTS_NS.nValue := GVL_APP.nTS_NS;
    GVL_PILS.stUTCCORREL1252P1.nValue := GVL_APP.nUTCCORREL1252P1;
    GVL_PILS.stUTCEL1252N1.nValue := GVL_APP.nUTCEL1252N1;
    GVL_PILS.stSystemUTCtime.nValue := EL6688_PTP.nSystemUTCtime;
    GVL_PILS.stPTPTimeOffsetDiffEL6688.nValue := EL6688_RW_CoeExtTimeOffset.nTimeOffsetDiffEL6688;
    GVL_PILS.stPTPState.nValue  := EL6688_PTP.nPtpState;
    // The raw values, 2 ways to calculate the offset
    IF GVL_PILS.stPTPDoSyncCheck.nTargetValue <> 0 THEN
	    GVL_PILS.stPTPOffset_0.nValue := EL6688_SynchCheck.nPTPOffset_2;
    ELSE
	    GVL_PILS.stPTPOffset_0.nValue := EL6688_PTP.nPTPOffset_1;
	END_IF
    GVL_PILS.stPTPOffset_0.nStatus := GVL_APP.nPTPErrorStatusBits;

	IF NOT bPILSV2isInitialized THEN
	    GVL_PILS.stPTPDoSyncCheck.nTargetValue := SEL(GVL_APP.bPTP_IOC_DoSyncCheckDefault, 0, 1);
	    GVL_PILS.stPTPRewriteOffset.nTargetValue := SEL(GVL_APP.bPTP_IOC_RewriteOffsetDefault, 0, 1);
	END_IF
    GVL_PILS.stPTPRewriteOffset.nCurrentValue := GVL_PILS.stPTPRewriteOffset.nTargetValue;
    GVL_PILS.stPTPDoSyncCheck.nCurrentValue := GVL_PILS.stPTPDoSyncCheck.nTargetValue;

    GVL_PILS.stPTPSyncSeqNum.nValue  := UINT_TO_INT(INT_TO_UINT(EL6688_PTP.nPtpSyncSeqNum) AND 7); // Just see if it is counting
    GVL_PILS.stPTPErrorStatus.nValue := UDINT_TO_DINT(GVL_APP.nPTPErrorStatusBits  AND 16#0FFFFFFF);
	GVL_PILS.stPTPErrorStatus.nStatus := GVL_APP.nPTPErrorStatusBits;
    GVL_PILS.stPTPDcToExtTimeOffsetSystem.nValue := GVL_APP.nDcToExtTimeOffset;

    GVL_PILS.stDcTime64.nValue               := EL6688_PTP.nDcTime64;

    GVL_PILS.stVelAct1.fValue                := astAxes[1].stStatus.fActVelocity;
    // Encoder
    GVL_PILS.stEL5101CounterValue.nValue := GVL_APP.nEL5101CounterValue;
    //GVL_PILS.stEL5101CounterValue.nValue := TINC^NC-Task 1 SAF^Axes^Axis 1^Enc^Inputs^In^nDataIn1[0];
        // Stepper terminal
    GVL_PILS.stEL7037STM_Velocity.nValue := GVL_APP.nEL7037STM_Velocity;
    GVL_PILS.stEL7037STM_CounterValue.nValue := GVL_APP.nEL7037STM_CounterValue;

    // PLCOpen status: Do not change anything here, because EPICS
    // has the same definition
    nPLCopenStateMachine1.0 := astAxes[1].Axis.Status.StandStill;
    nPLCopenStateMachine1.1 := astAxes[1].Axis.Status.Homing;
    nPLCopenStateMachine1.2 := astAxes[1].Axis.Status.DiscreteMotion;
    nPLCopenStateMachine1.3 := astAxes[1].Axis.Status.ContinuousMotion;
    nPLCopenStateMachine1.4 := astAxes[1].Axis.Status.SynchronizedMotion;
    nPLCopenStateMachine1.5 := astAxes[1].Axis.Status.Stopping;
    nPLCopenStateMachine1.6 := astAxes[1].Axis.Status.ErrorStop;
    nPLCopenStateMachine1.7 := astAxes[1].Axis.Status.Disabled;
    GVL_PILS.stPLCopenBits1.nStatus:= WORD_TO_UINT(nPLCopenStateMachine1);

    //  AUX bits for motor 1
//    stMotorM1.nStatus.16;
//    stMotorM1.nStatus.15;
//    stMotorM1.nStatus.14;
//    stMotorM1.nStatus.13;
//    stMotorM1.nStatus.12;
    stMotorM1.nStatus.12 := astAxes[1].stControl.bHalt;
    stMotorM1.nStatus.11 := astAxes[1].stStatus.bMovingForward;
    stMotorM1.nStatus.10 := astAxes[1].stStatus.bMovingBackward;
    stMotorM1.nStatus.9 := astAxes[1].stControl.bExecute;
    stMotorM1.nStatus.8 := astAxes[1].stControl.bReset;
    stMotorM1.nStatus.7 := astAxes[1].stStatus.bBusy;
    stMotorM1.nStatus.6 := astAxes[1].stStatus.bDone;
    stMotorM1.nStatus.5 := astAxes[1].stControl.eCommand = E_MotionFunctions.eMoveAbsolute;
    stMotorM1.nStatus.4 := astAxes[1].stControl.eCommand = E_MotionFunctions.eMoveVelocity;
    stMotorM1.nStatus.3 := astAxes[1].stControl.eCommand = E_MotionFunctions.eHome;
    stMotorM1.nStatus.2 := astAxes[1].stStatus.bMoving;
    stMotorM1.nStatus.1 := astAxes[1].stStatus.bCommandAborted;
    stMotorM1.nStatus.0 := astAxes[1].stControl.eCommand = E_MotionFunctions.eWriteParameter OR
                               astAxes[1].stControl.eCommand = E_MotionFunctions.eReadParameter;
							   
    bPILSV2isInitialized := TRUE;							   
END_IF
IF MAIN.bInit THEN
    INDEXER_COMM();
END_IF
]]></ST>
    </Implementation>
  </POU>
</TcPlcObject>